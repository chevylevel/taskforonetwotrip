public class MatrixUnwrapperTest {

    public static void main(String[] args) {
        int[][] mtx = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        String assertion = "5 4 7 8 9 6 3 2 1";
        testUnwrapSpiral(MatrixUnwrapper.unwrapSpiral(mtx), assertion);
    }

    public static void testUnwrapSpiral(String result, String assertion) {
        if (result.equals(assertion)) {
            System.out.println("Pass");
        } else {
            System.out.println("Fail " + assertion + " is expected " + result + " - actual");
        }
    }
}