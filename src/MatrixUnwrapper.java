public class MatrixUnwrapper {

    public static String unwrapSpiral(int[][] mtx) {
        int mtxLength = mtx.length;
        int start = mtxLength / 2;
        int hor = start;
        int ver = start;
        int turn = 1;
        StringBuilder result = new StringBuilder();

        while (true) {
            // go to the left or to the right
            for (int j = turn; j > 0 && hor >= 0; j--) {
                System.out.print(mtx[ver][hor] + " ");
                result.append(String.valueOf(mtx[ver][hor])).
                        append(" ");
                if (turn % 2 == 0) {
                    hor++;
                } else hor--;
            }

            //last row goes to the left until hor < 0. if < 0 then finish.
            if (hor < 0) {
                break;
            }

            //go up or down
            for (int i = turn; i > 0; i--) {
                System.out.print(mtx[ver][hor] + " ");
                result.append(String.valueOf(mtx[ver][hor])).
                        append(" ");
                if (turn % 2 == 0) {
                    ver--;
                } else {
                    ver++;
                }
            }
            turn++;
        }
        return result.toString().trim();
    }
}