# Test task for OneTwoTrip

Script unwrap square matrix with odd elements in spiral order.

# Quick start

Example of script launch on Windows, jdk 1.8:

```
javac MatrixUnwrapper.java
javac MatrixUnwrapperTest.java
java MatrixUnwrapperTest
5 4 7 8 9 6 3 2 1 Pass

```

# Project Goals

The code has been written as test task for job vacancy.